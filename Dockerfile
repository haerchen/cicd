FROM nginx:1.20.0-alpine

RUN apk add --no-cache --update

EXPOSE 80
